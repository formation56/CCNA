# Exploring the functions of networking

**Réseau**
Ensemble de systèmes interconnectés qui partagent des ressources.
>- envoi de data
>- communique grâce aux adresses MAC
>- l'adresse MAC identifie un équipement
>- différents types de réseaux : LAN, WAN, MAN, etc..

**SOHO**
Smal Office Home Office
Ex : Connexion via une box à domicile ou petite entreprise

**BRANCH**
Sites distants
Ex : CAMPUS - HD - BRANCH

* * *

## Components of networking

**Internet/Cloud**
Equipements non maitrisés, hors LAN

**Routeur** 
Oriente les paquets via le routage, trouve les adresse IP de destination grâce à sa table de routage.

**Firewall**
Premier objet de sécurité, ensemble d'ACL.
Gère les flux entrants/sortants

**IPS**
Intrusion Prevention System
Securise les **flux au niveau de la data** grâce au DPI (Deep Packet Inspection)

**IDS**
Intrusion Detection System
Inspecte le traffic sans le bloquer.
Analyse fine via monitoring
Le but est d'améliorer les flux réseau

**Layer 3 SWITCH**
Fonction SW + routeur

**Layer 2 SWITCH**
Uniquement SW
Lit les trames ethernet, lit les adresses MAC SRC et DST
Le but est de permettre à plusieurs équipements d'être interconnecté sur un réseau.

**VM**
Virtual Machine
Virtualise les équipements réseau physiques.
Déporte l'intelligence sur une VM : Control Plane
La techno NFV (Network Function Virtualization) virtualise la fonction réseau (SW, Router, FW..) via une appliance.
Permet également de doubler les équipements comme les FW pour augmenter la sécurité.

**Management Services**
Ex : Cisco DNA
Manage l'infra depuis un seul point.
Pousse les config sur tous les équipements.

**WLC**
Wireless controller
Centralise la mobilité et pilote les AP (cfg, roaming, ACL,..)

**End Devices**
Ex : PC, téléphones, imprimantes, etc..
Génèrent et reçoivent de la data

**Media**
Cables cuivre, fibres, cables série, ondes wifi, etc..

**Services**
Mot d'ordre : standardisation (RFC, IEEE)
>- application (DNS, DHCP, Outlook, FTP)
>- Network control (IP)
>- Protocol implementation (RFC)

**Intermediary devices**
Switch, routeurs, firewall, WLC, IPS, etc..

* * *

## Caractéristiques d'un réseau

**Topology**
Design (qui est connecté à quoi)
Ex : MESH, Bus, étoile, etc..

**Bitrate ou bandwith**
Maitrise de la bande passante

**Availability**
Disponibilité d'un réseau vis à vis des pannes

**Reliability**
Fiabilité des équipements, liens, redondance, etc..

**Scalability**
Capacité à un réseau à croitre, à évoluer

**Security**
Design de la sécurité, avant, pendant et après une attaque

**QoS**
Prioriser certains traffics pour éviter les congestions, via les ACL.
De nos jours, les réseaux sont convergés : data, vidéo, voix

**Cost**
Adapter la qualité en fonction du budget

**Virtualization**
Permet de réduire les coûts, enjeu majeur actuellement

**Calcul de disponibilité**
Availability =  $\frac{UPTIME}{UPTIME + DOWNTIME}$

**Reliability Meantime Between Failure**
RMBF = $\frac{TIME IN SERVICE}{NUMBER OF FAILURE}$

* * *

## Topologies

**Bus**
Tous les équipements sont connectés via un seul cable, le switch est au milieu.
Tout le monde parle avec tout le monde.
<img src="/Images/Bus_topolog.png"  width="150" height="150">

**Ring**
Tous les équipements sont cablés successivement.
Tout le monde a deux voisins. Redondance de chemin.
<img src="/Images/ring_topology.png" width="150" height="150">



