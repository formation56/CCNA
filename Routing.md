## Routing Table
Table dans laquelle le routeur conserve les meilleures routes pour atteindre différents réseaux
> - type de connexion
> - distance administrative
> - métriques de protocoles de routage
> - etc..

## Administrative distance
Fonctionnalité que les routeurs utilisent pour sélectionner le meilleur chemin lorsqu'il y a 2 ou plusieurs routes différents, vers la même destination, à partir de protocoles de routage différents.

Elle définit la fiabilité d'un protocole de routage.

La distance administrative la plus basse est la meilleure.

| Route Source 	| Default distance values 	|
|:------------:	|:-----------------------:	|
| Connected IF 	|            0            	|
| Static Route 	|            1            	|
|      BGP     	|            20           	|
|     EIGRP    	|            90           	|
|     OSPF     	|           110           	|
|      RIP     	|           120           	|


## Metrics

Utilisé par un protocole de routage pour calculer le meilleur chemin vers une destination donnée.
Chaque protocole utilise une métrique différente
ex : OSPF utilise la BP + le cost
