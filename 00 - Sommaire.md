# PROGRAMME CCNA

## Day 1 
> - Exploring the **functions of networking**
> - Introducing the **host to host communication model**
> - Operating **CISCO IOS software**
> - Introducing **LANs**
> - Exploring TCPIP **Link Layer**

## Day 2
>- Starting a **Switch**
>- Introducing the TCPIP **Internet Layer, IPv4 and subnetting**
>- Explaining the TCPIP **Transport Layer** and **Application Layer**
>- Exploring the functions of **routing**
>- Configuring a Cisco **router**

## Day 3
>- Exploring the **Packet Delivery Process**
>- **Troubleshooting** a simple network
>- Introducing basic **IPv6**
>- Configuring **static routing**
>- Implementing **Vlans and trunks**

## Day 4
>- **Routing** between Vlans
>- Introducing **OSPF**
>- Improving redundant switched topologies with **Etherchannel**
>- Explaining the basics of **ACL**
>- Enabling Internet **connectivity**

## Day 5
>- Explaining the evolution of **intelligent networks**
>- Introducing **system monitoring**
>- Managing **Cisco devices**
>- **Securing** administratives access
>- Implementing device **hardening**

## Day 6-8 Self Study
>- Building **redundant switched topologies**
>- Exploring **Layer 3 redundancy**
>- Introducing **WAN technologies**
>- Introducing **QoS**
>- Explaining **Wireless Fundamentals**
>- Introducing **architectures and virtualization**
>- Examining the **security threat landscape**
>- Implementing **threat defense technologies**




